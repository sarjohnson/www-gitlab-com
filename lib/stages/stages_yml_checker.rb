require 'yaml'

module Stages
  class StagesYmlLinkChecker
    def run(stages_yml_path = nil)
      log "Running stages.yml Link Checker..."

      stages = load_stages_yml(stages_yml_path)

      error_messages = stages['stages'].each_with_object([]) do |(_stage, sdata), memo|
        memo << check_slack_channel('stage', sdata)

        sdata['groups'].flat_map do |group, gdata|
          memo << check_slack_channel('group', gdata)
        end
      end.flatten.compact

      log "\n"
      if error_messages.empty?
        log "SUCCESS: stages.yml links are valid, no bad stages/groups found."
      else
        err_msg = "ERROR: stages.yml had #{error_messages.length} errors"
        log "#{err_msg}:\n"
        error_messages.each_with_index do |msg, i|
          log "#{i + 1}. #{msg}"
        end
        log "\n"
        raise err_msg
      end
    end

    private

    def check_slack_channel(resource_type, stage_data)
      return if stage_data.dig('slack', 'channel')

      bad_resource(resource_type, stage_data, "missing slack.channel")
    end

    def load_stages_yml(stages_yml_path)
      stages_yml_path ||= File.expand_path('../../data/stages.yml', __dir__)
      stages = YAML.load_file(stages_yml_path)
      stages
    end

    def bad_resource(resource_type, resource, reason)
      "Bad #{resource_type}: '#{resource['name'] || resource['display_name']}': #{reason}."
    end

    def log(msg)
      puts msg
    end
  end
end
